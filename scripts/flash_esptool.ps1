<#
.SYNOPSIS
    Tool to program an ESP with a software based on the Arduino framework
.DESCRIPTION
    Tool to program the ESP of the LoraBlue
    The JLink programmer must be plugged to the ESP and the power button must be held pressed for the whole programmation procedure
.PARAMETER Port
    COM Port to use with esptool
.PARAMETER Binaries
    Path to binaries folder
    Must contains a boot.bin, part.bin and app.bin
#>

param(
    [Parameter(Mandatory=$true)][string]$Binaries,
    [Parameter(Mandatory=$true)][string]$Port
)

python -m esptool `
--port $Port `
--chip esp32 `
write_flash `
0x1000 $Binaries/boot.bin `
0x8000 $Binaries/part.bin `
0x10000 $Binaries/app.bin
