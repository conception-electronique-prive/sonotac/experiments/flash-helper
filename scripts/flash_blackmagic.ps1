<#
.SYNOPSIS
    Tool to program a nRF
.PARAMETER Gdb
    Path to gdb
    Default to arm-none-eabi-gdb.exe
    Must be the arm version
.PARAMETER Port
    COM port for the blackmagic probe
    Do NOT append \\.\ to your port
    ex: COM36
.PARAMETER Binary
    Path of the binary to flash
    ex: C:\MyProject\build\zephyr\zephyr.elf
#>

param(
    [string]$Gdb,
    [parameter(Mandatory=$true)][string]$Port,
    [Parameter(Mandatory=$true)][string]$Binary
)

if($Gdb -eq "") {
    $Gdb = arm-none-eabi-gdb.exe
}

$Port = "\\.\$Port"

& $Gdb --batch -nx -ex "target extended-remote $Port" -x $PSScriptRoot\bmp_flash.scr $Binary