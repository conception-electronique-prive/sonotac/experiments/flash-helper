<#
.SYNOPSIS
    Tool to program an STM32/GD32
.PARAMETER StmProgrammer
    Path to STM32 Programmer CLI tool.
    Default to STM32_Programmer_CLI.exe.
    Can be downloaded here: https://www.st.com/en/development-tools/stm32cubeprog.html (require an account)
#>

param(
    [string]$StmProgrammer
)



if($StmProgrammer -eq "") {
    $StmProgrammer = "STM32_Programmer_CLI.exe"
}

& $StmProgrammer --connect port=swd index=0 --erase all -rst