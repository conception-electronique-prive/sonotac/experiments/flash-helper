<#
.SYNOPSIS
    Tool to program an ESP with a software based on the Arduino framework
.DESCRIPTION
    Tool to program the ESP of the LoraBlue
    The JLink programmer must be plugged to the ESP and the power button must be held pressed for the whole programmation procedure
.PARAMETER OpenOcdPath
    Path to espressif open-ocd.
    Default to opencd-esp32.
    Can be downloaded here: https://github.com/espressif/openocd-esp32 (go to release)
.PARAMETER Binaries
    Path to binaries to flash
    Used to select the programmation folder (esp32 or lorablue)
#>

param(
    [string]$OpenOcdPath = "",
    [Parameter(Mandatory=$true)][string]$Binaries
)

if($OpenOcdPath -eq "") {
    $OpenOcdPath = $env:OpenOcdEsp32Path
}

$OpenOcdExe = "$OpenOcdPath/bin/openocd.exe"
$OpenOcdScripts = "$OpenOcdPath/share/openocd/scripts"

& $OpenOcdExe -d2 -s $OpenOcdScripts `
-f interface/ftdi/esp32_devkitj_v1.cfg `
-f board/esp-wroom-32.cfg `
-c "adapter speed 5000" `
-c "init; reset halt; flash erase_sector 0 1 1023" `
-c "program_esp $Binaries/boot.bin 0x1000 verify" `
-c "program_esp $Binaries/part.bin 0x8000 verify" `
-c "program_esp $Binaries/app.bin 0x10000 verify" `
-c "reset run; shutdown"