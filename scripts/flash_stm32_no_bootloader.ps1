<#
.SYNOPSIS
    Tool to program an STM32/GD32
.PARAMETER StmProgrammer
    Path to STM32 Programmer CLI tool.
    Default to STM32_Programmer_CLI.exe.
    Can be downloaded here: https://www.st.com/en/development-tools/stm32cubeprog.html (require an account)
.PARAMETER Binaries
    Path to the folder holding the binaries
    The folder must include both a bootloader.bin and a controller.bin
#>

param(
    [string]$StmProgrammer,
    [parameter(Mandatory=$true)][string]$Binaries
)

if($StmProgrammer -eq "") {
    $StmProgrammer = "STM32_Programmer_CLI.exe"
}

$Application = "$Binaries/app.bin"

& $StmProgrammer `
--connect port=swd index=0 `
--erase all `
--download $Application 0x08000000 -v `
-rst