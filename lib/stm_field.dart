import 'package:flash_helper/file_field.dart';
import 'package:flutter/material.dart';

class StmField extends StatefulWidget {
  final TextEditingController programmerPath;

  const StmField({super.key, required this.programmerPath});

  @override
  State<StmField> createState() => _StmFieldState();
}

class _StmFieldState extends State<StmField> {
  @override
  Widget build(BuildContext context) => Column(
        children: [
          Text("STM32", style: Theme.of(context).textTheme.titleMedium),
          FileField(
            name: "Stm Programmer",
            extensions: const ["exe"],
            controller: widget.programmerPath,
          )
        ],
      );
}
