enum ProgrammingInterface {
  unknown,
  serial,
  jtag;

  int toInt() => index;

  static ProgrammingInterface fromInt(int? index) {
    if (index == serial.index) return serial;
    if (index == jtag.index) return jtag;
    return unknown;
  }
}
