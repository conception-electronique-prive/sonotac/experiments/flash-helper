enum TargetKind {
  controller,
  remote;

  int getMaxStep() {
    switch (this) {
      case TargetKind.controller:
        return 4;
      case TargetKind.remote:
        return 3;
    }
  }
}
