import 'package:flash_helper/file_field.dart';
import 'package:flash_helper/serial_field.dart';
import 'package:flutter/material.dart';

class NrfField extends StatefulWidget {
  final TextEditingController gdbPath;
  final ValueNotifier<String?> port;

  const NrfField({
    super.key,
    required this.gdbPath,
    required this.port,
  });

  @override
  State<NrfField> createState() => _NrfFieldState();
}

class _NrfFieldState extends State<NrfField> {
  @override
  Widget build(BuildContext context) => Column(
        children: [
          Text("NRF52", style: Theme.of(context).textTheme.titleMedium),
          SerialField(port: widget.port),
          FileField(
            name: "GDB",
            extensions: const ["exe"],
            controller: widget.gdbPath,
          ),
        ],
      );
}
