import 'package:flutter/material.dart';
import "package:flutter_libserialport/flutter_libserialport.dart";
import 'package:gap/gap.dart';

class SerialField extends StatefulWidget {
  final ValueNotifier<String?> port;

  const SerialField({super.key, required this.port});

  @override
  State<SerialField> createState() => _SerialFieldState();
}

class _SerialFieldState extends State<SerialField> {
  List<String> _ports = [];

  @override
  void initState() {
    super.initState();
    _initPortList();
  }

  @override
  void didUpdateWidget(covariant SerialField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.port.value != widget.port.value) {
      _initPortList();
    }
  }

  void _initPortList() async {
    setState(() {
      _ports = SerialPort.availablePorts;
      if (!_ports.contains(widget.port.value)) widget.port.value = null;
      if (_ports.isEmpty) {
        _ports = [];
        widget.port.value = "";
      }
      if (widget.port.value == null && _ports.isNotEmpty) {
        widget.port.value = _ports.first;
      }
    });
  }

  @override
  Widget build(BuildContext context) => Row(
        children: [
          const Text("Port"),
          const Gap(8.0),
          if (_ports.isNotEmpty) ...[
            DropdownButton<String>(
              value: widget.port.value,
              items: List.generate(
                _ports.length,
                (index) => DropdownMenuItem(
                  value: _ports[index],
                  child: Text(_ports[index]),
                ),
              ),
              onChanged: (value) => setState(() => widget.port.value = value),
            ),
            IconButton(
              onPressed: _initPortList,
              icon: const Icon(Icons.refresh),
            ),
          ],
        ],
      );
}
