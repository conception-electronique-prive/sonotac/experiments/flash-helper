import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class McuEntry extends StatefulWidget {
  final String path;
  final String name;
  final bool directory;
  final void Function(String path) onPathChanged;

  const McuEntry({
    super.key,
    required this.path,
    required this.name,
    required this.directory,
    required this.onPathChanged,
  });

  @override
  State<McuEntry> createState() => _McuEntryState();
}

class _McuEntryState extends State<McuEntry> {
  late TextEditingController pathController =
      TextEditingController(text: widget.path);

  @override
  void dispose() {
    pathController.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(McuEntry oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.path != widget.path) {
      setState(() => pathController.text = widget.path);
    }
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(4.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: TextField(
                decoration: InputDecoration(
                  label: Text(widget.name),
                ),
                controller: pathController,
                onEditingComplete: () =>
                    widget.onPathChanged(pathController.text),
              ),
            ),
            const Gap(8.0),
            GestureDetector(
              onTap: widget.directory
                  ? () async {
                      String? directory =
                          await FilePicker.platform.getDirectoryPath();
                      if (directory != null) widget.onPathChanged(directory);
                    }
                  : () async {
                      FilePickerResult? result =
                          await FilePicker.platform.pickFiles(
                        allowMultiple: false,
                      );
                      if (result != null &&
                          result.files.length == 1 &&
                          result.paths[0] != null) {
                        widget.onPathChanged(result.paths[0]!);
                      }
                    },
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).colorScheme.primaryContainer,
                ),
                child: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.folder),
                ),
              ),
            )
          ],
        ),
      );
}
