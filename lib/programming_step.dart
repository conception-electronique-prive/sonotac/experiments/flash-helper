import 'package:flash_helper/target_kind.dart';

enum ProgrammingStep {
  eraseStm32,
  flashNrf52,
  flashEsp32,
  flashStm32,
  done;

  String getTitle() {
    switch (this) {
      case ProgrammingStep.eraseStm32:
        return "Effacement STM32";
      case ProgrammingStep.flashNrf52:
        return "Programmation NRF52";
      case ProgrammingStep.flashEsp32:
        return "Programmation ESP32";
      case ProgrammingStep.flashStm32:
        return "Programmation STM32";
      case ProgrammingStep.done:
        return "Terminé";
    }
  }

  ProgrammingStep getNext(TargetKind kind) {
    switch (this) {
      case ProgrammingStep.eraseStm32:
        return ProgrammingStep.flashNrf52;
      case ProgrammingStep.flashNrf52:
        return kind == TargetKind.controller
            ? ProgrammingStep.flashEsp32
            : ProgrammingStep.flashStm32;
      case ProgrammingStep.flashEsp32:
        return ProgrammingStep.flashStm32;
      case ProgrammingStep.flashStm32:
        return ProgrammingStep.done;
      case ProgrammingStep.done:
        return ProgrammingStep.eraseStm32;
    }
  }

  bool hasSuccessString(List<String> lines) {
    List<String> successLines = [];
    switch (this) {
      case ProgrammingStep.eraseStm32:
        successLines.add("Mass erase successfully achieved");
      case ProgrammingStep.flashNrf52:
        successLines.add(
            "Kill the program being debugged? (y or n) [answered Y; input not from terminal]");
      case ProgrammingStep.flashEsp32:
        successLines.add("Hash of data verified");
        successLines.add("shutdown command invoked");
      case ProgrammingStep.flashStm32:
        successLines.add("Download verified successfully");
      case ProgrammingStep.done:
        successLines.add("");
    }
    for (String line in lines) {
      for (String success in successLines) {
        if (line.contains(success)) return true;
      }
    }
    return false;
  }

  bool hasFailingString(List<String> lines) {
    List<String> failingLines = [];
    switch(this) {
      case ProgrammingStep.eraseStm32:
        failingLines.add("Error: No debug probe detected.");
      case ProgrammingStep.flashNrf52:
        failingLines.add("Attaching to Remote target failed");
        failingLines.add("\"monitor\" command not supported by this target");
      case ProgrammingStep.flashEsp32:
        failingLines.add("** OpenOCD init failed **");
      case ProgrammingStep.flashStm32:
        failingLines.add("Error: No debug probe detected.");
      case ProgrammingStep.done:
        // TODO: Handle this case.
    }
    for (String line in lines) {
      for (String success in failingLines) {
        if (line.contains(success)) return true;
      }
    }
    return false;
  }
}
