import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class FileField extends StatefulWidget {
  final String name;
  final List<String> extensions;
  final TextEditingController controller;

  const FileField({
    super.key,
    required this.name,
    required this.extensions,
    required this.controller,
  });

  @override
  State<FileField> createState() => _FileFieldState();
}

class _FileFieldState extends State<FileField> {
  void onFilePressed() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      dialogTitle: widget.name,
      type: FileType.custom,
      allowedExtensions: widget.extensions,
      allowMultiple: false,
      allowCompression: false,
    );
    if (result == null || result.paths.isEmpty) return;
    setState(() {
      widget.controller.text = result.paths.first ?? "";
      widget.controller.text = widget.controller.text.replaceAll("\\", "/");
    });
  }

  @override
  Widget build(BuildContext context) => Row(
        children: [
          Expanded(
            child: TextField(
              decoration: InputDecoration(
                label: Text(widget.name),
              ),
              controller: widget.controller,
              onEditingComplete: () {
                setState(() {
                  widget.controller.text =
                      widget.controller.text.replaceAll("\\", "/");
                });
              },
            ),
          ),
          const Gap(8.0),
          IconButton(
            onPressed: onFilePressed,
            icon: const Icon(Icons.folder),
          ),
        ],
      );
}
