import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flash_helper/console_viewer.dart';
import 'package:flash_helper/directory_field.dart';
import 'package:flash_helper/esp_field.dart';
import 'package:flash_helper/nrf_field.dart';
import 'package:flash_helper/programming_port.dart';
import 'package:flash_helper/programming_step.dart';
import 'package:flash_helper/round_container.dart';
import 'package:flash_helper/stm_field.dart';
import 'package:flash_helper/target_kind.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:process_run/shell.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainPanel extends StatefulWidget {
  final SharedPreferences sharedPreferences;

  const MainPanel({super.key, required this.sharedPreferences});

  @override
  State<MainPanel> createState() => _MainPanelState();
}

class _MainPanelState extends State<MainPanel> {
  TargetKind target = TargetKind.controller;

  TextEditingController scriptsDirectory = TextEditingController();
  TextEditingController binariesDirectory = TextEditingController();

  TextEditingController nrfGdbPath = TextEditingController();
  ValueNotifier<String?> nrfComPort = ValueNotifier<String?>(null);

  ValueNotifier<ProgrammingInterface> espInterface =
      ValueNotifier(ProgrammingInterface.jtag);
  TextEditingController espOpenOcd = TextEditingController();
  ValueNotifier<String?> espComPort = ValueNotifier<String?>(null);

  TextEditingController stmProgrammerPath = TextEditingController();

  List<String> consoleOutput = [];

  StreamController<List<int>> stdout = StreamController();
  StreamController<List<int>> stderr = StreamController();
  late Shell shell = Shell(
    runInShell: false,
    stdout: stdout,
    stderr: stderr,
  );
  bool flashing = false;
  bool failure = false;
  String failureExplanation = "";
  ProgrammingStep step = ProgrammingStep.eraseStm32;

  void resetProgramming() {
    setState(() {
      flashing = false;
      failure = false;
      failureExplanation = "";
      step = ProgrammingStep.eraseStm32;
      consoleOutput.clear();
    });
  }

  void onIntFieldUpdated(String name, int value) =>
      widget.sharedPreferences.setInt(name, value);

  void onStringFieldUpdated(String name, String value) =>
      widget.sharedPreferences.setString(name, value);

  @override
  void initState() {
    super.initState();
    setState(() {
      scriptsDirectory.text =
          widget.sharedPreferences.getString("scriptsDirectory") ?? "";
      binariesDirectory.text =
          widget.sharedPreferences.getString("binariesDirectory") ?? "";
      nrfGdbPath.text = widget.sharedPreferences.getString("nrfGdbPath") ?? "";
      nrfComPort.value = widget.sharedPreferences.getString("nrfComPort");
      espInterface.value = ProgrammingInterface.fromInt(
          widget.sharedPreferences.getInt("espProgrammingPort") ??
              ProgrammingInterface.jtag.index);
      espOpenOcd.text = widget.sharedPreferences.getString("espOpenOcd") ?? "";
      espComPort.value = widget.sharedPreferences.getString("espComPort");
      stmProgrammerPath.text =
          widget.sharedPreferences.getString("stmProgrammerPath") ?? "";
    });

    scriptsDirectory.addListener(
        () => onStringFieldUpdated("scriptsDirectory", scriptsDirectory.text));
    binariesDirectory.addListener(() =>
        onStringFieldUpdated("binariesDirectory", binariesDirectory.text));
    nrfGdbPath
        .addListener(() => onStringFieldUpdated("nrfGdbPath", nrfGdbPath.text));
    nrfComPort.addListener(
        () => onStringFieldUpdated("nrfComPort", nrfComPort.value ?? ""));
    espInterface.addListener(() =>
        onIntFieldUpdated("espProgrammingPort", espInterface.value.index));
    espOpenOcd
        .addListener(() => onStringFieldUpdated("espOpenOcd", espOpenOcd.text));
    espComPort.addListener(
        () => onStringFieldUpdated("espComPort", espComPort.value ?? ""));
    stmProgrammerPath.addListener(() =>
        onStringFieldUpdated("stmProgrammerPath", stmProgrammerPath.text));

    stdout.stream
        .transform(utf8.decoder)
        .transform(const LineSplitter())
        .listen((event) {
      setState(() => consoleOutput.add(event));
    });
    stderr.stream
        .transform(utf8.decoder)
        .transform(const LineSplitter())
        .listen((event) {
      setState(() => consoleOutput.add(event));
    });
  }

  @override
  void dispose() {
    scriptsDirectory.dispose();
    binariesDirectory.dispose();

    nrfGdbPath.dispose();
    nrfComPort.dispose();
    espInterface.dispose();
    espOpenOcd.dispose();
    espComPort.dispose();
    stmProgrammerPath.dispose();

    stdout.close();
    stderr.close();
    shell.kill();

    super.dispose();
  }

  bool canFlash() {
    if (flashing) return false;
    if (scriptsDirectory.text.isEmpty) return false;
    if (binariesDirectory.text.isEmpty) return false;
    if (nrfGdbPath.text.isEmpty) return false;
    if (nrfComPort.value == null || nrfComPort.value!.isEmpty) return false;
    if (target == TargetKind.controller) {
      if (espInterface.value == ProgrammingInterface.jtag) {
        if (espOpenOcd.text.isEmpty) return false;
      } else if (espInterface.value == ProgrammingInterface.serial) {
        if (espComPort.value == null || espComPort.value!.isEmpty) return false;
      } else {
        throw StateError("Esp Programming port is unknown");
      }
    }
    if (stmProgrammerPath.text.isEmpty) return false;
    return true;
  }

  void flash() async {
    if (step == ProgrammingStep.done) resetProgramming();
    setState(() {
      flashing = true;
      failure = false;
      failureExplanation = "";
    });
    switch (target) {
      case TargetKind.controller:
        flashController();
        break;
      case TargetKind.remote:
        flashRemote();
        break;
    }
  }

  List<ProcessResult> onFlashDone(List<ProcessResult> results) {
    stdout.add("\r\n".codeUnits);
    if (results.length != 1) throw Error();
    ProcessResult result = results[0];
    if (!step.hasSuccessString(result.outLines.toList()) &&
        !step.hasSuccessString(result.errLines.toList())) {
      throw "La console n'a pas affiché un succès. "
          "Vérifier la présence d'erreur dans la console. "
          "Si pas d'erreur, aller embeter l'equipe de développement logiciel";
    }
    if (step.hasFailingString(result.outLines.toList()) ||
        step.hasFailingString(result.errLines.toList())) {
      throw "Un rapport d'échec a été détecté. "
          "Vérifier la présence d'erreur dans la console. "
          "Si pas d'erreur, aller embeter l'equipe de développement logiciel";
    }
    for (final outLine in result.outLines) {
      if (outLine.contains("fatal error")) {
        throw outLine;
      }
    }
    setState(() => step = step.getNext(target));
    return results;
  }

  void onFlashError(Object? error, StackTrace? stacktrace) {
    if (failure) return;
    stderr.add("Failed to flash controller\r\n".codeUnits);
    stderr.add("$error\r\n".codeUnits);
    stderr.add("$stacktrace\r\n".codeUnits);
    setState(() {
      flashing = false;
      failure = true;
      failureExplanation = "$error";
    });
  }

  void reportStep() {
    setState(() {});
  }

  void flashController() async {
    try {
      if (step == ProgrammingStep.eraseStm32) {
        await shell
            .run(
              "pwsh "
              "-File \"${scriptsDirectory.text}\\erase_stm32.ps1\" "
              "-StmProgrammer \"${stmProgrammerPath.text}\"",
            )
            .then(
              onFlashDone,
              onError: onFlashError,
            );
      }

      if (step == ProgrammingStep.flashNrf52) {
        await shell
            .run(
              "pwsh "
              "-File \"${scriptsDirectory.text}/flash_blackmagic.ps1\" "
              "-Gdb \"${nrfGdbPath.text}\" "
              "-Port ${nrfComPort.value!} "
              "-Binary \"${binariesDirectory.text}/controller/nrf52.elf\"",
            )
            .then(
              onFlashDone,
              onError: onFlashError,
            );
      }

      if (step == ProgrammingStep.flashEsp32) {
        String command;
        if (espInterface.value == ProgrammingInterface.jtag) {
          command = "pwsh "
              "-File \"${scriptsDirectory.text}/flash_esp_generic_prog.ps1\" "
              "-OpenOcdPath \"${espOpenOcd.text}\" "
              "-Binaries \"${binariesDirectory.text}/controller/esp32\"";
        } else {
          command = "pwsh "
              "-File \"${scriptsDirectory.text}/flash_esptool.ps1\" "
              "-Port ${espComPort.value!} "
              "-Binaries \"${binariesDirectory.text}/controller/esp32\"";
        }

        await shell.run(command).then(
              onFlashDone,
              onError: onFlashError,
            );
      }

      if (step == ProgrammingStep.flashStm32) {
        await shell
            .run(
              "pwsh "
              "-File \"${scriptsDirectory.text}\\flash_stm32.ps1\" "
              "-StmProgrammer \"${stmProgrammerPath.text}\" "
              "-Binaries \"${binariesDirectory.text}/controller/stm32\"",
            )
            .then(
              onFlashDone,
              onError: onFlashError,
            );
      }

      setState(() {
        flashing = false;
      });
    } catch (error, stacktrace) {
      onFlashError(error, stacktrace);
    }
  }

  void flashRemote() async {
    try {
      if (step == ProgrammingStep.eraseStm32) {
        await shell
            .run(
              "pwsh "
              "-File ${scriptsDirectory.text}\\erase_stm32.ps1 "
              "-StmProgrammer \"${stmProgrammerPath.text}\"",
            )
            .then(
              onFlashDone,
              onError: onFlashError,
            );
      }

      if (step == ProgrammingStep.flashNrf52) {
        await shell
            .run(
              "pwsh "
              "-File ${scriptsDirectory.text}/flash_blackmagic.ps1 "
              "-Gdb ${nrfGdbPath.text} "
              "-Port ${nrfComPort.value!} "
              "-Binary ${binariesDirectory.text}/remote/nrf52.elf",
            )
            .then(
              onFlashDone,
              onError: onFlashError,
            );
      }

      if (step == ProgrammingStep.flashStm32) {
        String script = target == TargetKind.controller
            ? "flash_stm32.ps1"
            : "flash_stm32_no_bootloader.ps1";
        await shell
            .run(
              "pwsh "
              "-File ${scriptsDirectory.text}\\$script "
              "-StmProgrammer \"${stmProgrammerPath.text}\" "
              "-Binaries ${binariesDirectory.text}\\remote\\stm32",
            )
            .then(
              onFlashDone,
              onError: onFlashError,
            );
      }

      setState(() {
        flashing = false;
      });
    } catch (error, stacktrace) {
      onFlashError(error, stacktrace);
    }
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(4.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: RoundContainer(
                child: AbsorbPointer(
                  absorbing: flashing,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              "Appareil",
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                            const Gap(16),
                            ToggleButtons(
                              isSelected: [
                                target == TargetKind.controller,
                                target == TargetKind.remote,
                              ],
                              onPressed: (index) => setState(
                                  () => target = TargetKind.values[index]),
                              children: const [
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Text("Controlleur"),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Text("Manette"),
                                ),
                              ],
                            ),
                          ],
                        ),
                        DirectoryField(
                          name: "Scripts",
                          controller: scriptsDirectory,
                        ),
                        DirectoryField(
                          name: "Binaires",
                          controller: binariesDirectory,
                        ),
                        const Gap(16),
                        const Divider(),
                        const Gap(16),
                        NrfField(gdbPath: nrfGdbPath, port: nrfComPort),
                        if (target == TargetKind.controller) ...[
                          const Gap(16),
                          const Divider(),
                          const Gap(16),
                          EspField(
                            port: espComPort,
                            interface: espInterface,
                            openOcd: espOpenOcd,
                          ),
                        ],
                        const Gap(16),
                        const Divider(),
                        const Gap(16),
                        StmField(programmerPath: stmProgrammerPath),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  RoundContainer(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Programmation",
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                        const Gap(8.0),
                        Text("Étape: ${step.getTitle()}"),
                        if (flashing)
                          CircularProgressIndicator(
                            value: step.index / ProgrammingStep.done.index,
                          ),
                        if (step == ProgrammingStep.done) ...[
                          const Gap(8.0),
                          const Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.check_circle,
                                color: Colors.green,
                              ),
                              Text("Programmation terminée",
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18)),
                            ],
                          ),
                        ],
                        if (failure) ...[
                          const Gap(8.0),
                          const Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.warning,
                                color: Colors.red,
                              ),
                              Text("Erreur",
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18)),
                            ],
                          ),
                          Text(failureExplanation),
                        ],
                        const Gap(8.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            ElevatedButton(
                              onPressed: canFlash() ? flash : null,
                              child: Text(failure ? "Reprendre" : "Lancer"),
                            ),
                            if (failure)
                              ElevatedButton(
                                onPressed: canFlash() ? resetProgramming : null,
                                child: const Text("Reinitialiser"),
                              ),
                            if (flashing)
                              ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    flashing = false;
                                    failure = true;
                                    failureExplanation =
                                        "Programmations interrompue par l'utilisateur";
                                    shell.kill();
                                  });
                                },
                                child: const Text("Arrêter"),
                              )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: RoundContainer(
                      child: ConsoleViewer(
                        lines: consoleOutput.reversed.toList(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}
