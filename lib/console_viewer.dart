import 'package:flutter/material.dart';

class ConsoleViewer extends StatelessWidget {
  final List<String> lines;

  const ConsoleViewer({super.key, required this.lines});

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Text(
            "Console",
            style: Theme.of(context).textTheme.titleMedium,
          ),
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                  // color: Colors.black,
                  ),
              child: lines.isNotEmpty
                  ? Padding(
                      padding: const EdgeInsets.only(left: 2, right: 2),
                      child: ListView.builder(
                        reverse: true,
                        itemCount: lines.length,
                        itemBuilder: (context, index) =>
                            SelectableText(lines[index]),
                        // controller: controller,
                      ),
                    )
                  : null,
            ),
          ),
        ],
      );
}
