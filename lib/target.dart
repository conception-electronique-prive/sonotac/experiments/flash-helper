import 'package:flutter/material.dart';

abstract class TargetWidget extends StatelessWidget {
  static const mcuNameWidth = 300;
  static const filepathColumnWidth = 600;
  final List<String> files;
  final void Function(List<String> files) onFilesChanged;

  const TargetWidget({super.key, required this.files, required this.onFilesChanged});
}
