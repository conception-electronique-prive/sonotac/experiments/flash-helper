import 'dart:io';

import 'package:flutter/material.dart';
import 'package:process_run/process_run.dart';

class ProcessResultViewer extends StatelessWidget {
  final ProcessResult processResult;

  const ProcessResultViewer(this.processResult, {super.key});

  @override
  Widget build(BuildContext context) => Table(
        columnWidths: const {
          0: FixedColumnWidth(100),
        },
        children: [
          TableRow(children: [
            const Text("Exit Code"),
            Text(processResult.exitCode.toString()),
          ]),
          TableRow(children: [
            const Text("STDOUT"),
            Text(processResult.stdout.toString()),
          ]),
          TableRow(
            children: [
              const Text("STDERR"),
              Text(processResult.stderr),
            ],
          ),
          TableRow(
            children: [
              const Text("Outlines"),
              Text(processResult.outLines.toList().join("\r\n"))
            ]
          )
        ],
      );
}
