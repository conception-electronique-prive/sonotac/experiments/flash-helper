
import 'package:flash_helper/directory_field.dart';
import 'package:flash_helper/programming_port.dart';
import 'package:flash_helper/serial_field.dart';
import 'package:flutter/material.dart';

class EspField extends StatefulWidget {
  final TextEditingController openOcd;
  final ValueNotifier<String?> port;
  final ValueNotifier<ProgrammingInterface> interface;

  const EspField({
    super.key,
    required this.interface,
    required this.port,
    required this.openOcd,
  });

  @override
  State<EspField> createState() => _EspFieldState();
}

class _EspFieldState extends State<EspField> {
  @override
  Widget build(BuildContext context) => Column(
        children: [
          Text("ESP32", style: Theme.of(context).textTheme.titleMedium),
          ToggleButtons(
            isSelected: [
              widget.interface.value == ProgrammingInterface.jtag,
              widget.interface.value == ProgrammingInterface.serial
            ],
            onPressed: (index) {
              setState(() {
                widget.interface.value = index == 0
                    ? ProgrammingInterface.jtag
                    : ProgrammingInterface.serial;
              });
            },
            children: const [
              Text("JTAG"),
              Text("Série"),
            ],
          ),
          if (widget.interface.value == ProgrammingInterface.serial)
            SerialField(port: widget.port),
          if (widget.interface.value == ProgrammingInterface.jtag)
            DirectoryField(
              controller: widget.openOcd,
              name: "OpenOCD ESP32",
            )
        ],
      );
}
