import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class DirectoryField extends StatefulWidget {
  final String name;
  final TextEditingController controller;

  const DirectoryField({
    super.key,
    required this.name,
    required this.controller,
  });

  @override
  State<DirectoryField> createState() => _DirectoryFieldState();
}

class _DirectoryFieldState extends State<DirectoryField> {
  void onFolderPressed() async {
    String? folder = await FilePicker.platform.getDirectoryPath(
      initialDirectory: widget.controller.text,
      dialogTitle: widget.name,
    );
    if (folder == null) return;
    setState(() {
      widget.controller.text = folder;
      widget.controller.text = widget.controller.text.replaceAll("\\", "/");
    });
  }

  @override
  Widget build(BuildContext context) => Row(
        children: [
          Expanded(
            child: TextField(
              decoration: InputDecoration(
                label: Text(widget.name),
              ),
              controller: widget.controller,
              onEditingComplete: () {
                setState(() {
                  widget.controller.text =
                      widget.controller.text.replaceAll("\\", "/");
                });
              },
            ),
          ),
          const Gap(8.0),
          IconButton(
            onPressed: onFolderPressed,
            icon: const Icon(Icons.folder),
          ),
        ],
      );
}
