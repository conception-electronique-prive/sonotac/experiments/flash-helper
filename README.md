# Logiciel de programmation Waverbed

Utilitaire de production pour la programmation des produits Waverbed, controleur et manette.

## Utilisation
Ce logiciel vous permet de programmer automatiquement un controleur ou une manette du waverbed.
Pour fonctionner, vous devez d'abord fournir les informations necessaire au logiciel pour fonctionner.

### Configuration
1. Appareil  
Permet de selectionner le type d'appareil vous souhaitez programmer

2. Scripts  
Chemin vers le dossier contenant les scripts de programmation

3. Binaires  
Chemin vers le dossier contenant les binaries de programmation

4. NRF52 - Port  
Port série utilisé par la sonde BlackMagic

5. NRF52 - GDB  
Chemin vers l'executable arm-none-eabi-gdb.exe

6. ESP32 - Interface  
Interface de programmation utilisée. Dans notre cas, JTAG

7. ESP32 - OpenOCD (Interface jtag uniquement)  
Chemin vers le dossier OpenOCD pour ESP32

8. ESP32 - Port (Interface série uniquement)  
Port série utilisé pour l'ESP-Prog

### Programmation
Utiliser le bouton Lancer en haut à droite pour démarrer une programmation.

Cette dernière se déroule en 4 étapes
1. Nettoyage de la mémoire du STM32
2. Programmation du nRF52
3. Programmation de l'ESP32 (Controleur uniquement)
4. Programmation du STM32

Si la programmation s'est bien passée, la mention Programmation terminée devrait s'afficher.

En cas d'erreur, un message d'erreur vous informe du problème rencontré. Utilisez la console pour déterminer les raisons du problème ou bien contactez l'équipe de développement logiciel.

Lors d'une erreur, l'utilisateur va automatiquement reprendre d'où il était. Si vous souhaitez reprendre la programmation du début, utilisez le bouton `Réinitialiser`

## Requis
### Logiciels
* [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html)
* esptools `python -m pip install esptools` pour programmation ESP via UART
* [OpenOCD-ESP32](https://github.com/espressif/openocd-esp32/releases) pour programmation ESP via JTAG
* [ARM GCC toolchain](https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads)

### Programmeurs
* STM32 - STLink
* nRF52 - BlackMagic probe
* ESP32 - Esp-Prog


#### ESP32
pour programmer l'ESP32 en JTAG, le pilote de l'ESP-Prog doit supporter winusb.
Pour ce faire, utiliser l'utilitaire [Zadig](https://zadig.akeo.ie/) puis installer le pilote WinUSB.

Par défaut, zadig ne liste pas tous les péripheriques connectés. Utiliser l'option `List all devices` comme montré dans l'image ci-dessous
![zadig_list_all_devices](doc/images/zadig_list_all_devices.png)

Sélectionner l'interface `Dual RS232-HS (Interface 0)` comme dans l'image ci-dessous
⚠️ l'esp-prog et la carte GSSLAB rs232-uart utilisent la même puce FTDI.
veuillez retirer toute carte GSSLAB connectée à votre ordinateur pendant cette manipulation.
![zadig_rs232](doc/images/zadig_select_rs232-hs.png)

Remplacer le pilote FTDIBUS par le pilote WinUSB en cliquant sur le bouton Replace Driver
![zadig_replace_driver](doc/images/zadig_install_driver.png)
